import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      var appWidget = new MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Scaffold(


            /*-----------------------------------------BOTTOM NAVIGATION BAR-----------------------------------------*/
            bottomNavigationBar: BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Home',
                ),

                BottomNavigationBarItem(
                  icon: Icon(Icons.business),
                  label: 'Business',
                ),

                BottomNavigationBarItem(
                  icon: Icon(Icons.school),
                  label: 'School',
                ),

              ],
            ),

            /*-----------------------------------------APP BAR------------------------------------- */
            appBar: AppBar(
              backgroundColor: Colors.deepOrangeAccent,
              title: Text('Tiki'),
            ),

            /*-----------------------------------------BODY-----------------------------------------*/
            body:
            new Center(
              child: Text('This is center body'),
            ),

            /*-----------------------------------------BOTTOM SHEET-----------------------------------------*/
            bottomSheet: Container(
                height: 55,
                color: Colors.cyan[50],
                child:Column    (
                  children: [
                    Row (
                      children: [
                        Icon(Icons.place),
                        SizedBox(width:5, height:5),
                        Text("199 Valencia St, San Francisco, CA")
                      ],
                    ),
                    Row (
                      children: [
                        Icon(Icons.phone),
                        SizedBox(width:5, height:5),
                        Text("(415) 339-0942)")
                      ],
                    )
                  ],
                )
            ),

            /*-----------------------------------------FLOATING ACTION BUTTON-----------------------------------------*/
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                print('This is float action button');
              },
            ),

            /*-----------------------------------------PERSISTEN FOOTER BUTTONS-----------------------------------------*/
            persistentFooterButtons :[
              TextButton.icon(icon: Icon(Icons.map), label: Text(""), onPressed: () {}),
              TextButton.icon(icon: Icon(Icons.view_week), label: Text(""), onPressed: () {}),
            ],

            /*-----------------------------------------DRAWER-----------------------------------------*/
            endDrawer: Drawer(
              child: ListView(
                children: const <Widget> [
                  DrawerHeader(
                    decoration: BoxDecoration(
                      color: Colors.green,
                    ),
                    child: Text(
                      'Hello World',
                      style: TextStyle(
                        color: Colors.green,
                        fontSize: 24,
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text('Gallery'),
                  ),
                  ListTile(
                    title: Text('Slideshow'),
                  ),
                ],
              ),
            ),


          ),
        );
      return appWidget;
  }
}
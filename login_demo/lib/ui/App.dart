import 'package:flutter/material.dart';

/* Class app kế thừa statelessWidget */
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      /* debugShowCheckedModeBanner dùng để xóa cái chữ Debug trên góc phải màn hình */
      debugShowCheckedModeBanner: false,
      title: "Tiki Login demo",

      /* Scafford là để chứa các thành phần trong layout gồm body, appbar ,.... */
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }

}

/* Class LoginScreen là màn hình chính kế thừa statefulwidget*/
class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
  
}
/* Class LoginState kế thừa statefulwidget để chứa các widget như Thanh nhập email, password, button*/
class LoginState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Column(
      children: [
        emailField(),
        passwordField(),
        Container(margin: EdgeInsets.only(top:10),),
        loginbutton(),
        ],
      ),
    );
  }

  /* Thanh nhập Email adrress */
  Widget emailField() {
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email address'
      ),
    );
  }

  /* Thanh nhập password */
  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
    );
  }

  /* Nút Login */
  Widget loginbutton() {
    return ElevatedButton(
      onPressed: () {
        print('YOU HAVE CLICK THE BUTTON');
          },
      child: Text('GO'),
    );
  }
}
